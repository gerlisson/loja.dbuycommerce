#!/bin/bash
if [ -n "$1" ] && [ $1 == "--homolog" ];
then
    file="$(dirname $0)/hml-sites.txt"
else
    file="$(dirname $0)/prod-sites.txt"
fi
while IFS='' read -r line || [[ -n "$line" ]];
do
    curl -s "$line/clear-cache/trigger?key-cache=$AUTHORIZATION_CACHE_KEY"
    printf "\n$line clear \n"
done <"$file"
