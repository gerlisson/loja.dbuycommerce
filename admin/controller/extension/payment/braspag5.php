<?php
require_once DIR_SYSTEM.'../app/braspag5/libs.php';

class ControllerExtensionPaymentBraspag5 extends Controller
{
    public $opcoes = [];
    private $error = [];

    public function index()
    {
        $this->load->language('extension/payment/braspag5');
        $this->document->setTitle('Cielo API 3.0');
        $this->load->model('setting/setting');
        if ('POST' === $this->request->server['REQUEST_METHOD'] && $this->validate()) {
            $this->model_setting_setting->editSetting('payment_braspag5', $this->request->post);
            if (isset($this->request->post['payment_braspag5_status_tef'])) {
                $this->model_setting_setting->editSetting('payment_braspag5tef', ['payment_braspag5tef_status' => $this->request->post['payment_braspag5_status_tef']]);
            }

            if (isset($this->request->post['payment_braspag5_status_boleto'])) {
                $this->model_setting_setting->editSetting('payment_braspag5boleto', ['payment_braspag5boleto_status' => $this->request->post['payment_braspag5_status_boleto']]);
            }

            if (isset($this->request->post['payment_braspag5_status_debito'])) {
                $this->model_setting_setting->editSetting('payment_braspag5debito', ['payment_braspag5debito_status' => $this->request->post['payment_braspag5_status_debito']]);
            }

            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/payment/braspag5', 'salvo=true&user_token='.$this->session->data['user_token'], 'SSL'));
        }

        $data['campos'] = $this->getCustomFields();
        $data['provides'] = ['Cielo', 'Redecard', 'RedeSitef', 'CieloSitef', 'SantanderSitef'];
        $data['arraybancos'] = ['Bradesco' => 'Bradesco (cielo/braspag)', 'Itau' => 'Ita&uacute; (braspag)', 'BancoDoBrasil' => 'Banco do Brasil (cielo/braspag)'];
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($_GET['salvo'])) {
            $data['salvo'] = true;
        } else {
            $data['salvo'] = false;
        }

        if (isset($_GET['pedidos'])) {
            $data['pedidos'] = true;
        } else {
            $data['pedidos'] = false;
        }

        $data['breadcrumbs'] = [];
        $data['breadcrumbs'][] = ['text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'user_token='.$this->session->data['user_token'], 'SSL')];
        $data['breadcrumbs'][] = ['text' => $this->language->get('text_payment'), 'href' => $this->url->link('marketplace/extension', 'type=payment&user_token='.$this->session->data['user_token'], 'SSL')];
        $data['breadcrumbs'][] = ['text' => 'Braspag / Cielo Webservice 3.0', 'href' => $this->url->link('extension/payment/braspag5', 'user_token='.$this->session->data['user_token'], 'SSL')];
        $data['action'] = $this->url->link('extension/payment/braspag5', 'user_token='.$this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('marketplace/extension', 'type=payment&user_token='.$this->session->data['user_token'], 'SSL');
        $data['token'] = $this->session->data['user_token'];
        $q = $this->db->query('SELECT * FROM '.DB_PREFIX.'braspag5_pedidos ORDER BY id_braspag DESC LIMIT 2000');
        $data['pedidos_cielo'] = $q->rows;
        $serial = trim($this->config->get('payment_braspag5_serial'));
        $key_string = $serial;
        $remote_auth = 'df08acba4ec3';
        $key_location = DIR_SYSTEM.'../app/braspag5/key.php';
        $key_age = 1296000;
        $resultado = new gateway_braspag5_open_web_loja5($key_string, $remote_auth, $key_location, $key_age);
        $idPagamento = base64_decode($resultado->result, true);
        $data['mod_ativado'] = $idPagamento;
        $this->get('payment_braspag5_serial');
            $this->get('payment_braspag5_status');
            $this->get('payment_braspag5_geo_zone_id');
            $this->get('payment_braspag5_modo');
            $this->get('payment_braspag5_id');
            $this->get('payment_braspag5_senha');
            $this->get('payment_braspag5_cpf');
            $this->get('payment_braspag5_numero');
            $this->get('payment_braspag5_complemento');
            $this->get('payment_braspag5_debug');
            $this->get('payment_braspag5_operadora');
            $this->get('payment_braspag5_status_cartao');
            $this->get('payment_braspag5_titulo_cartao');
            $this->get('payment_braspag5_ordem_cartao');
            $this->get('payment_braspag5_total_cartao');
            $this->get('payment_braspag5_div');
            $this->get('payment_braspag5_sem');
            $this->get('payment_braspag5_meios');
            $this->get('payment_braspag5_minimo');
            $this->get('payment_braspag5_juros');
            $this->get('payment_braspag5_tipo_parcelamento');
            $this->get('payment_braspag5_tipo_captura');
            $this->get('payment_braspag5_soft');
            $this->get('payment_braspag5_provide_cartao');
            $this->get('payment_braspag5_status_debito');
            $this->get('payment_braspag5_titulo_debito');
            $this->get('payment_braspag5_ordem_debito');
            $this->get('payment_braspag5_total_debito');
            $this->get('payment_braspag5_provide_debito');
            $this->get('payment_braspag5_status_boleto');
            $this->get('payment_braspag5_titulo_boleto');
            $this->get('payment_braspag5_ordem_boleto');
            $this->get('payment_braspag5_total_boleto');
            $this->get('payment_braspag5_provide_boleto');
            $this->get('payment_braspag5_boleto_cedente');
            $this->get('payment_braspag5_boleto_instrucoes');
            $this->get('payment_braspag5_boleto_demo');
            $this->get('payment_braspag5_boleto_dias');
            $this->get('payment_braspag5_status_tef');
            $this->get('payment_braspag5_titulo_tef');
            $this->get('payment_braspag5_ordem_tef');
            $this->get('payment_braspag5_total_tef');
            $this->get('payment_braspag5_provide_tef');
            $this->get('payment_braspag5_in');
            $this->get('payment_braspag5_pg');
            $this->get('payment_braspag5_ca');
            $this->get('payment_braspag5_ne');
            $this->get('payment_braspag5_au');
            $this->get('payment_braspag5_pe');
            $this->get('payment_braspag5_antifraude');
            $this->get('payment_braspag5_orgid');
            $this->get('payment_braspag5_mid');
            $tema = 'extension/payment/braspag5';

        foreach ($this->opcoes as $k => $v) {
            $data[$k] = $v;
        }
        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $this->load->model('localisation/geo_zone');
        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view($tema, $data));
    }

    public function uninstall()
    {
        $this->load->model('setting/extension');
        $this->model_setting_extension->uninstall('payment', 'braspag5tef');
        $this->model_setting_extension->uninstall('payment', 'braspag5boleto');
        $this->model_setting_extension->uninstall('payment', 'braspag5debito');
    }

    public function getCustomFields($data = [])
    {
        if (empty($data['filter_customer_group_id'])) {
            $sql = 'SELECT * FROM `'.DB_PREFIX.'custom_field` cf LEFT JOIN '.DB_PREFIX."custom_field_description cfd ON (cf.custom_field_id = cfd.custom_field_id) WHERE cfd.language_id = '".(int) $this->config->get('config_language_id')."'";
        } else {
            $sql = 'SELECT * FROM '.DB_PREFIX.'custom_field_customer_group cfcg LEFT JOIN `'.DB_PREFIX.'custom_field` cf ON (cfcg.custom_field_id = cf.custom_field_id) LEFT JOIN '.DB_PREFIX."custom_field_description cfd ON (cf.custom_field_id = cfd.custom_field_id) WHERE cfd.language_id = '".(int) $this->config->get('config_language_id')."'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND cfd.name LIKE '".$this->db->escape($data['filter_name'])."%'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $sql .= " AND cfcg.customer_group_id = '".(int) $data['filter_customer_group_id']."'";
        }

        $sort_data = ['cfd.name', 'cf.type', 'cf.location', 'cf.status', 'cf.sort_order'];
        if (isset($data['sort']) && in_array($data['sort'], $sort_data, true)) {
            $sql .= ' ORDER BY '.$data['sort'];
        } else {
            $sql .= ' ORDER BY cfd.name';
        }

        if (isset($data['order']) && 'DESC' === $data['order']) {
            $sql .= ' DESC';
        } else {
            $sql .= ' ASC';
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= ' LIMIT '.(int) $data['start'].','.(int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function orderAction()
    {
        return $this->orderCielo();
    }

    public function order()
    {
        return $this->orderCielo();
    }

    public function action()
    {
        return $this->orderCielo();
    }

    public function orderCielo()
    {
        $existe = $this->getOrderCielo($this->request->get['order_id']);
        if (isset($existe['link']) && !empty($existe['link'])) {
            $data['operadora'] = ucfirst($this->config->get('payment_braspag5_operadora'));
            $data['cielo'] = $this->getOrderCielo($this->request->get['order_id']);
            $data['link_consulta'] = $this->url->link('extension/payment/braspag5/consultar', 'user_token='.$this->session->data['user_token'].'&pedido='.$this->request->get['order_id'].'&tid='.$existe['link'], 'SSL');

            return $this->load->view('extension/payment/braspag5_acoes', $data);
        }
    }

    public function cap()
    {
        if ('braspag' === $this->config->get('payment_braspag5_operadora')) {
            if ('0' === $this->config->get('payment_braspag5_modo')) {
                $url_cielo = 'https://apisandbox.braspag.com.br/v2/';
            } else {
                $url_cielo = 'https://api.braspag.com.br/v2/';
            }
        } else {
            if ('0' === $this->config->get('payment_braspag5_modo')) {
                $url_cielo = 'https://apisandbox.cieloecommerce.cielo.com.br/1/';
            } else {
                $url_cielo = 'https://api.cieloecommerce.cielo.com.br/1/';
            }
        }

        $headers = ['Content-Type' => 'application/json', 'Accept' => 'application/json', 'MerchantId' => trim($this->config->get('payment_braspag5_id')), 'MerchantKey' => trim($this->config->get('payment_braspag5_senha')), 'RequestId' => ''];
        $api = new RestClient(['base_url' => $url_cielo, 'headers' => $headers]);
        $dados['amount'] = $_GET['amount'];
        $response = $api->put('sales/'.$_GET['tid'].'/capture', json_encode($dados));
        if (isset($response->status)) {
            $dados_pedido = $this->obj2array($response->response);
            if (200 === $response->status || 201 === $response->status) {
                $data['order_status_id'] = $this->config->get('payment_braspag5_pg');
                $data['comment'] = 'Pagamento Confirmado';
                $data['notify'] = true;
                $this->db->query('UPDATE `'.DB_PREFIX."braspag5_pedidos` SET status = 2 WHERE `id_pedido` = '".(int) $_GET['pedido']."'");
                $this->addOrderHistory((int) $_GET['pedido'], $data);
                echo '<script>window.opener.location.reload(true);</script><script>window.close();</script>';
            } else {
                $log = '<center><b>Erro ao Capturar Pagamento!</b><br>';
                if (is_array($dados_pedido)) {
                    foreach ($dados_pedido as $k => $v) {
                        $log .= $v['Code'].' - '.$v['Message'].'<br>';
                    }
                }

                exit($log);
            }
        }
    }

    public function can()
    {
        if ('braspag' === $this->config->get('payment_braspag5_operadora')) {
            if ('0' === $this->config->get('payment_braspag5_modo')) {
                $url_cielo = 'https://apisandbox.braspag.com.br/v2/';
            } else {
                $url_cielo = 'https://api.braspag.com.br/v2/';
            }
        } else {
            if ('0' === $this->config->get('payment_braspag5_modo')) {
                $url_cielo = 'https://apisandbox.cieloecommerce.cielo.com.br/1/';
            } else {
                $url_cielo = 'https://api.cieloecommerce.cielo.com.br/1/';
            }
        }

        $headers = ['Content-Type' => 'application/json', 'Accept' => 'application/json', 'MerchantId' => trim($this->config->get('payment_braspag5_id')), 'MerchantKey' => trim($this->config->get('payment_braspag5_senha')), 'RequestId' => ''];
        $api = new RestClient(['base_url' => $url_cielo, 'headers' => $headers]);
        $dados['amount'] = $_GET['amount'];
        $response = $api->put('sales/'.$_GET['tid'].'/void', json_encode($dados));
        if (isset($response->status)) {
            $dados_pedido = $this->obj2array($response->response);
            if (200 === $response->status || 201 === $response->status) {
                $data['order_status_id'] = $this->config->get('payment_braspag5_ca');
                $data['comment'] = 'Pedido Cancelado';
                $data['notify'] = true;
                $this->db->query('UPDATE `'.DB_PREFIX."braspag5_pedidos` SET status = 10 WHERE `id_pedido` = '".(int) $_GET['pedido']."'");
                $this->addOrderHistory((int) $_GET['pedido'], $data);
                echo '<script>window.opener.location.reload(true);</script><script>window.close();</script>';
            } else {
                $log = '<center><b>Erro ao Cancelar Pagamento!</b><br>';
                if (is_array($dados_pedido)) {
                    foreach ($dados_pedido as $k => $v) {
                        $log .= $v['Code'].' - '.$v['Message'].'<br>';
                    }
                }

                exit($log);
            }
        }
    }

    public function consultar()
    {
        if ('braspag' === $this->config->get('payment_braspag5_operadora')) {
            if ('0' === $this->config->get('payment_braspag5_modo')) {
                $url_cielo = 'https://apiquerysandbox.braspag.com.br/v2/';
            } else {
                $url_cielo = 'https://apiquery.braspag.com.br/v2/';
            }
        } else {
            if ('0' === $this->config->get('payment_braspag5_modo')) {
                $provider = 'Simulado';
                $url_cielo = 'https://apiquerysandbox.cieloecommerce.cielo.com.br/1/';
            } else {
                $url_cielo = 'https://apiquery.cieloecommerce.cielo.com.br/1/';
                $provider = trim($this->config->get('payment_braspag5_provide_boleto'));
            }
        }

        $headers = ['Content-Type' => 'application/json', 'Accept' => 'application/json', 'MerchantId' => trim($this->config->get('payment_braspag5_id')), 'MerchantKey' => trim($this->config->get('payment_braspag5_senha')), 'RequestId' => ''];
        $api = new RestClient(['base_url' => $url_cielo, 'headers' => $headers]);
        $response = $api->get('sales/'.$_GET['tid'].'');
        $data['operadora'] = ucfirst($this->config->get('payment_braspag5_operadora'));
        if (isset($response->status)) {
            $dados_pedido = $this->obj2array($response->response);
            if (200 === $response->status || 201 === $response->status) {
                $pagamento = $dados_pedido['Payment'];
                echo "<html>\n\t\t\t<head>\n\t\t\t<script src=\"//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js\"></script>\n\t\t\t<link href=\"//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css\" rel=\"stylesheet\">\n\t\t\t<title>Detalhes</title>\n\t\t\t</head>\n\t\t\t<body>\n\t\t\t<div class=\"container\">\n\t\t\t<center>\n\t\t\t<h3>Detalhes Pedido #".(int) $_GET['pedido']."</h3>\n\t\t\t<b>Status ".$data['operadora'].':</b> '.$pagamento['Status'].' - '.$this->getStatus($pagamento['Status']).'<br>';
                echo '<b>Meio:</b> '.$pagamento['Type'].' '.$pagamento['Provider'].'<br>';
                echo '<b>PaymentId:</b> '.$pagamento['PaymentId'].'<br>';
                if (isset($pagamento['Tid'])) {
                    echo '<b>TID:</b> '.$pagamento['Tid'].'<br>';
                }

                if (isset($pagamento['AcquirerTransactionId'])) {
                    echo '<b>TID:</b> '.$pagamento['AcquirerTransactionId'].'<br>';
                }

                if (isset($pagamento['ProofOfSale'])) {
                    echo '<b>ProofOfSale:</b> '.$pagamento['ProofOfSale'].'<br>';
                }

                if (isset($pagamento['AuthorizationCode'])) {
                    echo '<b>AuthorizationCode:</b> '.$pagamento['AuthorizationCode'].'<br>';
                }

                if (isset($pagamento['Amount'])) {
                    echo '<b>Total:</b> '.number_format($pagamento['Amount'] / 100, 2, '.', '').'<br>';
                }

                if (isset($pagamento['CapturedAmount'])) {
                    echo '<b>Total Capturado:</b> '.number_format($pagamento['CapturedAmount'] / 100, 2, '.', '').' em '.$pagamento['CapturedDate'].'<br>';
                }

                if (isset($pagamento['Installments'])) {
                    echo '<b>Parcela(s):</b> '.$pagamento['Installments'].'x<br>';
                }

                if (isset($pagamento['CreditCard'])) {
                    echo '<b>Bandeira:</b> '.$pagamento['CreditCard']['Brand'].' ('.$pagamento['CreditCard']['CardNumber'].')<br>';
                }

                if (isset($pagamento['DigitableLine'])) {
                    echo '<b>Linha Digitavel:</b> '.$pagamento['DigitableLine'].'x<br>';
                }

                if (isset($pagamento['ExpirationDate'])) {
                    echo '<b>Validade:</b> '.$pagamento['ExpirationDate'].'<br>';
                }

                if (isset($pagamento['Url'])) {
                    echo '<a href="'.$pagamento['Url'].'" target="_blank">Link de Pagamento</a><br>';
                }

                if (isset($pagamento['Type']) && (0 === $pagamento['Status'] || 1 === $pagamento['Status']) && ('Boleto' === $pagamento['Type'] || 'EletronicTransfer' === $pagamento['Type'])) {
                    echo "<br><a onclick=\"return confirm('Deseja marca pago o pedido?');\" href=\"index.php?route=extension/payment/braspag5/consultar&user_token=".$_GET['user_token'].'&pedido='.$_GET['pedido'].'&tid='.$_GET['tid'].'&pago=true&amount='.$pagamento['Amount'].'">Marca Pago</a> ou ';
                    echo "<a onclick=\"return confirm('Deseja marca cancelado o pedido?');\" href=\"index.php?route=extension/payment/braspag5/consultar&user_token=".$_GET['user_token'].'&pedido='.$_GET['pedido'].'&tid='.$_GET['tid'].'&cancelar=true&amount='.$pagamento['Amount'].'">Cancelar</a><br>';
                }

                if (isset($pagamento['Type']) && (1 === $pagamento['Status'] || 2 === $pagamento['Status']) && ('CreditCard' === $pagamento['Type'] || 'DebitCard' === $pagamento['Type'])) {
                    $prazo_can = (int) (15 * 60 * 60 * 24);
                    $data_compra = strtotime($pagamento['ReceivedDate']);
                    $agora = time();
                    $tempo = (int) $agora - $data_compra;
                    $rem2 = ($data_compra + $prazo_can) - $agora;
                    $day2 = floor($rem2 / 86400);
                    $hr2 = floor($rem2 % 86400 / 3600);
                    $min2 = floor($rem2 % 3600 / 60);
                    $can = $this->url->link('extension/payment/braspag5/can', 'user_token='.$this->session->data['user_token'].'&pedido='.$_GET['pedido'].'&amount='.$pagamento['Amount'].'&tid='.$_GET['tid'], 'SSL');
                    if ($tempo < $prazo_can) {
                        echo '<br><a href="'.$can."\" class=\"btn btn-small btn-danger\" onclick=\"return confirm('Deseja cancelar o pedido #".$_GET['pedido']."?');\">Cancelar (".$day2.' dias '.$hr2.'rs '.$min2.'min)</a> ';
                    }
                }

                if (isset($pagamento['Type']) && 1 === $pagamento['Status'] && ('CreditCard' === $pagamento['Type'] || 'DebitCard' === $pagamento['Type'])) {
                    $prazo_can = (int) (5 * 60 * 60 * 24);
                    $data_compra = strtotime($pagamento['ReceivedDate']);
                    $agora = time();
                    $tempo = (int) $agora - $data_compra;
                    $rem2 = ($data_compra + $prazo_can) - $agora;
                    $day2 = floor($rem2 / 86400);
                    $hr2 = floor($rem2 % 86400 / 3600);
                    $min2 = floor($rem2 % 3600 / 60);
                    $can = $this->url->link('extension/payment/braspag5/cap', 'user_token='.$this->session->data['user_token'].'&pedido='.$_GET['pedido'].'&amount='.$pagamento['Amount'].'&tid='.$_GET['tid'], 'SSL');
                    if ($tempo < $prazo_can) {
                        echo '<br><br><a href="'.$can."\" class=\"btn btn-small btn-success\" onclick=\"return confirm('Deseja capturar o pedido #".$_GET['pedido']."?');\">Capturar (".$day2.' dias '.$hr2.'rs '.$min2.'min)</a> ';
                    }
                }
            } else {
                echo 'Ocorre um problema ao consultar a transa&ccedil;&atilde;o! Atualiza a p&aacute;gina.';
                echo '<pre>'.print_r($response->response, true).'</pre>';
            }
        } else {
            echo 'Ocorre um problema ao consultar a transa&ccedil;&atilde;o! Atualiza a p&aacute;gina.<br>';
            echo '<pre>'.print_r($response, true).'</pre>';
        }

        if (isset($_GET['cancelar'])) {
            $data['order_status_id'] = $this->config->get('payment_braspag5_ca');
            $data['comment'] = 'Pedido Cancelado';
            $data['notify'] = true;
            $this->db->query('UPDATE `'.DB_PREFIX."braspag5_pedidos` SET status = 10 WHERE `id_pedido` = '".(int) $_GET['pedido']."'");
            $this->addOrderHistory((int) $_GET['pedido'], $data);
            echo '<script>window.opener.location.reload(true);</script><script>window.close();</script>';
        }

        if (isset($_GET['pago'])) {
            $data['order_status_id'] = $this->config->get('payment_braspag5_pg');
            $data['comment'] = 'Pedido Pago';
            $data['notify'] = true;
            $this->db->query('UPDATE `'.DB_PREFIX."braspag5_pedidos` SET status = 2 WHERE `id_pedido` = '".(int) $_GET['pedido']."'");
            $this->addOrderHistory((int) $_GET['pedido'], $data);
            echo '<script>window.opener.location.reload(true);</script><script>window.close();</script>';
        }
    }

    public function getOrderCielo($order_id)
    {
        $order_query = $this->db->query('SELECT * FROM `'.DB_PREFIX."braspag5_pedidos` WHERE `id_pedido` = '".(int) $order_id."' ORDER BY id_braspag DESC");

        return $order_query->row;
    }

    public function obj2array($obj)
    {
        return @json_decode($obj, true);
    }

    public function get($campo)
    {
        if (isset($this->request->post[$campo])) {
            $this->opcoes[$campo] = (is_array($this->request->post[$campo]) ? implode(',', $this->request->post[$campo]) : $this->request->post[$campo]);
        } else {
            $this->opcoes[$campo] = $this->config->get($campo);
        }
    }

    public function install()
    {
        $this->load->model('setting/extension');
        $this->model_setting_extension->install('payment', 'braspag5tef');
        $this->model_setting_extension->install('payment', 'braspag5boleto');
        $this->model_setting_extension->install('payment', 'braspag5debito');
        $this->db->query('CREATE TABLE IF NOT EXISTS `'.DB_PREFIX."braspag5_pedidos` (\n\t\t\t`id_braspag` INT(15) NOT NULL AUTO_INCREMENT,\n\t\t\t`id_pedido` INT(15) NOT NULL,\n\t\t\t`total_pedido` FLOAT(10,2) NOT NULL,\n\t\t\t`total_pago` FLOAT(10,2) NOT NULL,\n\t\t\t`bandeira` VARCHAR(20) NOT NULL,\n\t\t\t`parcelas` INT(10) NOT NULL,\n\t\t\t`tid` VARCHAR(30) NOT NULL,\n\t\t\t`lr` VARCHAR(5) NOT NULL,\n\t\t\t`lr_log` TEXT NOT NULL,\n\t\t\t`status` INT(10) NOT NULL,\n\t\t\t`bin` VARCHAR(20) NOT NULL,\n\t\t\t`tipo` ENUM('credito','debito', 'tef', 'boleto') NOT NULL,\n\t\t\t`link` VARCHAR(255) NOT NULL,\n\t\t\t`data` DATETIME NOT NULL,\n\t\t\tPRIMARY KEY (`id_braspag`)\n\t\t)\n\t\tCOLLATE='latin1_swedish_ci'\n\t\tENGINE=InnoDB\n\t\tAUTO_INCREMENT=17\n\t\t;");
    }

    public function addOrderHistory($order_id, $data)
    {
        $this->db->query('UPDATE `'.DB_PREFIX."order` SET order_status_id = '".(int) $data['order_status_id']."', date_modified = NOW() WHERE order_id = '".(int) $order_id."'");
        $this->db->query('INSERT INTO '.DB_PREFIX."order_history SET order_id = '".(int) $order_id."', order_status_id = '".(int) $data['order_status_id']."', notify = '".((isset($data['notify']) ? (int) $data['notify'] : 0))."', comment = '".$this->db->escape(strip_tags($data['comment']))."', date_added = NOW()");
        $this->load->model('sale/order');
        $order_info = $this->model_sale_order->getOrder($order_id);
        if ($data['notify']) {
            $subject = sprintf('%s - Pedido %s Atualizado', $order_info['store_name'], $order_id);
            $message = 'Pedido: #'.' '.$order_id."\n";
            $message .= 'Adicionada em '.' '.date('d/m/Y H:i:s', strtotime($order_info['date_added']))."\n\n";
            $order_status_query = $this->db->query('SELECT * FROM '.DB_PREFIX."order_status WHERE order_status_id = '".(int) $data['order_status_id']."' AND language_id = '".(int) $order_info['language_id']."'");
            if ($order_status_query->num_rows) {
                $message .= 'Status atual:'."\n";
                $message .= $order_status_query->row['name']."\n\n";
            }

            if ($order_info['customer_id']) {
                $message .= 'Para visualizar detalhes do pedido acesse:'."\n";
                $message .= html_entity_decode($order_info['store_url'].'index.php?route=account/order/info&order_id='.$order_id, ENT_QUOTES, 'UTF-8')."\n\n";
            }

            if ($data['comment']) {
                $message .= 'Comentario:'."\n\n";
                $message .= strip_tags(html_entity_decode($data['comment'], ENT_QUOTES, 'UTF-8'))."\n\n";
            }

            $message .= '';
            $mail = new Mail($this->config->get('config_mail_engine'));
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
            $mail->setTo($order_info['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender($order_info['store_name']);
            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
            @$mail->send();
        }
    }

    public function getStatus($status)
    {
        switch ($status) {
            case '0':
                $status = 'Aguardando Pagamento';

                break;
            case '1':
                $status = 'Autorizado';

                break;
            case '2':
                $status = 'Pagamento Confirmado';

                break;
            case '3':
                $status = 'Negado';

                break;
            case '20':
                $status = 'Agendado';

                break;
            case '13':
                $status = 'Abortado';

                break;
            case '12':
                $status = 'Pendente';

                break;
            case '11':
                $status = 'Devolvido';

                break;
            case '10':
                $status = 'Cancelado';

                break;
            default:
                $status = 'N&atilde;o Finalizado';

                break;
        }

        return $status;
    }

    public function getLr($lrcod)
    {
        switch ($lrcod) {
            case '00':
                $lrcod = '00 - Transacao autorizada';

                break;
            case '01':
            case '04':
            case '07':
            case '41':
            case '62':
            case '63':
                $lrcod = $lrcod.' - Transacao referida/com restricao pelo emissor (Contate o emissor de seu cartao)';

                break;
            case '05':
                $lrcod = '05 - Transacao nao autorizada';

                break;
            case '12':
            case '82':
                $lrcod = $lrcod.' - Transacao invalida';

                break;
            case '13':
                $lrcod = '13 - Valor invalido';

                break;
            case '14':
                $lrcod = '14 - Cartao invalido';

                break;
            case '15':
                $lrcod = '15 - Emissor invalido';

                break;
            case '51':
                $lrcod = '51 - Saldo insuficiente';

                break;
            case '54':
                $lrcod = '54 - Cartao vencido';

                break;
            case '57':
            case '58':
                $lrcod = $lrcod.' - Transacao nao permitida';

                break;
            case '78':
                $lrcod = '78 - Cartao nao desbloqueado (Contate o emissor de seu cartao)';

                break;
            case '91':
                $lrcod = '91 - Banco indisponivel';

                break;
            case 'AC':
                $lrcod = 'AC - Cartao de debito tentando seu usado como credito.';

                break;
            default:
                $lrcod = $lrcod;
        }

        return $lrcod;
    }

    protected function validate()
    {
        return true;
    }
}

class gateway_braspag5_open_web_loja5
{
    public $license_key;
    public $home_url_site = 'd3d3LmxvY2FzaXN0ZW1hcy5jb20=';
    public $home_url_port = 80;
    public $home_url_iono = 'L2NsaWVudGVzL3JlbW90ZS5waHA=';
    public $key_location;
    public $remote_auth;
    public $key_age;
    public $key_data;
    public $now;
    public $result;

    public function __construct($license_key, $remote_auth, $key_location, $key_age)
    {
        $this->gateway_braspag5_open_web_loja5($license_key, $remote_auth, $key_location, $key_age);
    }

    public function Visa()
    {
        return true;
    }

    public function http_response($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://'.$url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $httpCode;
    }

    public function CiCielo()
    {
        return true;
    }

    public function MCoder($str)
    {
        return base64_decode($str, true);
    }

    public function gateway_braspag5_open_web_loja5($license_key, $remote_auth, $key_location = 'key.cielo5.php', $key_age = 1296000)
    {
        $this->license_key = $license_key;
        $this->remote_auth = $remote_auth;
        $this->key_location = $key_location;
        $this->key_age = $key_age;
        $this->now = time();
        $p_id = '35';
        $a = @explode('-', $license_key);
        if (isset($a[2]) && $a[2] !== $p_id) {
            $this->result = 'NjY=';

            return true;
        }

        $servidoronline = $this->http_response($this->MCoder($this->home_url_site));
        if ($servidoronline <= 0) {
            $this->result = 'MQ==';

            return true;
        }

        if (empty($license_key)) {
            $this->result = 'NA==';

            return false;
        }

        if (empty($remote_auth)) {
            $this->result = 'NA==';

            return false;
        }

        if (file_exists($this->key_location)) {
            $this->result = $this->PagamentoCreditoDebitoCielo();
        } else {
            $this->result = $this->PagamentoCreditoDebito();
            if (empty($this->result)) {
                $this->result = $this->PagamentoCreditoDebitoCielo();
            }
        }

        $this->remote_auth = null;

        return true;
    }

    public function PagamentoCreditoDebito()
    {
        $servidoronline = $this->http_response($this->MCoder($this->home_url_site));
        if ($servidoronline <= 0) {
            return 'MQ==';
        }

        $request = 'remote=licenses&type=5&license_key='.urlencode(base64_encode($this->license_key));
        $request .= '&host_ip='.urlencode(base64_encode('')).'&host_name='.urlencode(base64_encode(str_replace('www.', '', $_SERVER['SERVER_NAME'])));
        $request .= '&hash='.urlencode(base64_encode(md5($request)));
        $request = $this->MCoder($this->home_url_iono).'?'.$request;
        $header = 'GET '.$request." HTTP/1.0\r\nHost: ".$this->MCoder($this->home_url_site)."\r\nConnection: Close\r\nUser-Agent: iono (www.olate.co.uk/iono)\r\n";
        $header .= "\r\n\r\n";
        $fpointer = @fsockopen(@$this->MCoder($this->home_url_site), $this->home_url_port, $errno, $errstr, 5);
        $return = '';
        if ($fpointer) {
            @fwrite($fpointer, $header);
            while (!@feof($fpointer)) {
                $return .= @fread($fpointer, 1024);
            }
            @fclose($fpointer);
            $content = explode("\r\n\r\n", $return);
            $content = explode($content[0], $return);
            $string = urldecode($content[1]);
            $exploded = explode('|', $string);
            switch ($exploded[0]) {
                case 0:
                    return 'OA==';
                case 2:
                    return 'OQ==';
                case 3:
                    return 'NQ==';
                case 10:
                    return 'NA==';
            }
            list(, $data['license_key'], $data['expiry'], $data['hostname'], $data['ip']) = $exploded;
            $data['timestamp'] = $this->now;
            if (empty($data['hostname'])) {
                $data['hostname'] = str_replace('www.', '', $_SERVER['SERVER_NAME']);
            }

            if (empty($data['ip'])) {
                $data['ip'] = '';
            }

            $data_encoded = serialize($data);
            $data_encoded = base64_encode($data_encoded);
            $data_encoded = md5($this->now.$this->remote_auth).$data_encoded;
            $data_encoded = strrev($data_encoded);
            $data_encoded_hash = sha1($data_encoded.$this->remote_auth);
            $fp = fopen($this->key_location, 'w');
            if ($fp) {
                $fp_write = fwrite($fp, wordwrap($data_encoded.$data_encoded_hash, 40, "\n", true));
                if (!$fp_write) {
                    return 'MTE=';
                }

                fclose($fp);
            } else {
                return 'MTA=';
            }
        } else {
            return 'MTI=';
        }
    }

    public function B0000000()
    {
        return true;
    }

    public function PagamentoCreditoDebitoCielo()
    {
        $key = file_get_contents($this->key_location);
        if (false !== $key) {
            $key = str_replace("\n", '', $key);
            $key_string = substr($key, 0, strlen($key) - 40);
            $key_sha_hash = substr($key, strlen($key) - 40, strlen($key));
            if (sha1($key_string.$this->remote_auth) === $key_sha_hash) {
                $key = strrev($key_string);
                $key_hash = substr($key, 0, 32);
                $key_data = substr($key, 32);
                $key_data = base64_decode($key_data, true);
                $key_data = unserialize($key_data);
                if (md5($key_data['timestamp'].$this->remote_auth) === $key_hash) {
                    if ($this->key_age <= $this->now - $key_data['timestamp']) {
                        unlink($this->key_location);
                        $this->result = $this->PagamentoCreditoDebito();
                        if (empty($this->result)) {
                            $this->result = $this->PagamentoCreditoDebitoCielo();
                        }

                        return 'MQ==';
                    }

                    $this->key_data = $key_data;
                    if ($key_data['license_key'] !== $this->license_key) {
                        return 'NA==';
                    }

                    if ($key_data['expiry'] <= $this->now && 1 !== $key_data['expiry']) {
                        return 'NQ==';
                    }

                    if (0 === substr_count($key_data['hostname'], ',')) {
                        $limpo = str_replace('www.', '', $_SERVER['SERVER_NAME']);
                        if ($key_data['hostname'] !== $limpo && !empty($key_data['hostname'])) {
                            return 'Ng==';
                        }
                    } else {
                        $hostnames = explode(',', $key_data['hostname']);
                        $limpo = str_replace('www.', '', $_SERVER['SERVER_NAME']);
                        if (!in_array($limpo, $hostnames, true)) {
                            return 'Ng==';
                        }
                    }

                    return 'MQ==';
                }

                return 'Mw==';
            }

            return 'Mg==';
        }

        return 'MA==';
    }

    public function get_data()
    {
        return $this->key_data;
    }

    public function B39()
    {
        return true;
    }
}

?>