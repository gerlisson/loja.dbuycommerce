<?php 

class ControllerExtensionPaymentPagseguro extends Controller {
 	public function index() {
	
		$this->language->load('extension/payment/pagseguro');
		
		require_once(DIR_SYSTEM . 'library/PagSeguroLibrary/PagSeguroLibrary.php');
		
		// Altera a codificação padrão da API do PagSeguro (ISO-8859-1)
		PagSeguroConfig::setApplicationCharset('UTF-8');
		
		if($this->config->get('payment_pagseguro_ambiente') == 'sandbox') {
			PagSeguroConfig::setEnvironment('sandbox');
		}
		
    	$this->load->model('checkout/order');
		$this->load->model('extension/payment/pagseguro');
		
	    $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$data['success_redirect'] = $this->url->link('checkout/success');
		$data['cancel_redirect'] = $this->url->link('checkout/cart');

		if($order_info) {
			
			$paymentRequest = new PagSeguroPaymentRequest();
			
			/* 
			 * Dados do cliente
			 */
			 
			// Ajuste no nome do comprador para o máximo de 50 caracteres exigido pela API
			$customer_name = trim($order_info['payment_firstname']) . ' ' . trim($order_info['payment_lastname']);
			
			// evita erros invalid senderName
			$customer_name = preg_replace('/\d/', '', $customer_name);
			$customer_name = preg_replace('/[\n\t\r]/', ' ', $customer_name);
			$customer_name = preg_replace('/\s(?=\s)/', '', $customer_name);
			$customer_name = trim($customer_name);
			$customer_name = explode(' ', $customer_name);
			 
			if(count($customer_name) == 1 ) {
				$customer_name[] = 'da Silva';
			}
			$customer_name = implode(' ', $customer_name);			
			// fim			
			
			if($order_info['currency_code'] != "BRL"){
				$this->log->write("PagSeguro :: Pedido " . $this->session->data['order_id'] . ". O PagSeguro só aceita moeda BRL (Real) e a loja está configurada para a moeda " . $order_info['currency_code']);
			}
			
			$customer_name = $this->aparar($customer_name, 50);			
			
			$paymentRequest->setCurrency($order_info['currency_code']);
			$paymentRequest->setSenderName($customer_name);
			$paymentRequest->setSenderEmail(trim($order_info['email'])); // há limitação de 60 caracteres de acordo com a API
			
			$this->load->model('account/custom_field');
			$custom_fields = $this->model_account_custom_field->getCustomFields();
			
			$cpf = '';
			
			if($this->config->get('payment_pagseguro_campo_cpf') != "" && isset($order_info[$this->config->get('payment_pagseguro_campo_cpf')]) && $order_info[$this->config->get('payment_pagseguro_campo_cpf')] != ''){
				$cpf = html_entity_decode($order_info[$this->config->get('payment_pagseguro_campo_cpf')], ENT_QUOTES, 'UTF-8');
			} else if($this->config->get('payment_pagseguro_campo_cpf') != ""){
				$cpf_temp = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_cpf'));
				
				if($cpf_temp){
					$cpf = html_entity_decode($cpf_temp, ENT_QUOTES, 'UTF-8');
				}
			}
			
			if ($this->isCPF($cpf)) {
				$paymentRequest->addSenderDocument('CPF', preg_replace ("/[^0-9]/", '', $cpf));
			}
			
			// novo
			$cnpj = '';
			
			if($this->config->get('payment_pagseguro_campo_cnpj') != "" && isset($order_info[$this->config->get('payment_pagseguro_campo_cnpj')]) && $order_info[$this->config->get('payment_pagseguro_campo_cnpj')] != ''){
				$cnpj = html_entity_decode($order_info[$this->config->get('payment_pagseguro_campo_cnpj')], ENT_QUOTES, 'UTF-8');
			} else if($this->config->get('payment_pagseguro_campo_cnpj') != ""){
				$cnpj_temp = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_cnpj'));
				
				if($cnpj_temp){
					$cnpj = html_entity_decode($cnpj_temp, ENT_QUOTES, 'UTF-8');
				}
			}
			
			if ($this->isCNPJ($cnpj)) {
				$paymentRequest->addSenderDocument('CNPJ', preg_replace ("/[^0-9]/", '', $cnpj));
			}			

			$ddd = '';
			
			if($this->config->get('payment_pagseguro_campo_ddd') != "" && isset($order_info[$this->config->get('payment_pagseguro_campo_ddd')]) && $order_info[$this->config->get('payment_pagseguro_campo_ddd')] != ''){
				$ddd = html_entity_decode($order_info[$this->config->get('payment_pagseguro_campo_ddd')], ENT_QUOTES, 'UTF-8');
			} else if($this->config->get('payment_pagseguro_campo_ddd') != ""){
				$ddd_temp = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_ddd'));
				
				if($ddd_temp){
					$ddd = html_entity_decode($ddd_temp, ENT_QUOTES, 'UTF-8');
				}
			}
			
			$ddd = preg_replace("/[^0-9]/", '', $ddd);

			$telephone = $order_info['telephone'];
			
			if($this->config->get('payment_pagseguro_campo_celular') != "" && isset($order_info[$this->config->get('payment_pagseguro_campo_celular')]) && $order_info[$this->config->get('payment_pagseguro_campo_celular')] != ''){
				$telephone = html_entity_decode($order_info[$this->config->get('payment_pagseguro_campo_celular')], ENT_QUOTES, 'UTF-8');
			} else if($this->config->get('payment_pagseguro_campo_celular') != ""){
				$telefone_temp = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_celular'));
				
				if($telefone_temp){
					$telephone = html_entity_decode($telefone_temp, ENT_QUOTES, 'UTF-8');
				}
			}

			$telephone = preg_replace("/[^0-9]/", '', $telephone);
			
			if(strlen($ddd) == 2 && strlen($telephone) >= 7 && strlen($telephone) <= 9) {
				$paymentRequest->setSenderPhone($ddd, $telephone);
			} else {
				$telephone = ltrim($telephone, '0');
				
				if(strlen($telephone) > 9) {
					$ddd = substr($telephone, 0, 2);
					$telephone = substr($telephone, 2, strlen($telephone) - 1);
					
					if(strlen($ddd) == 2 && strlen($telephone) >= 7 && strlen($telephone) <= 9) {
						$paymentRequest->setSenderPhone($ddd, $telephone);
					}					
				}
			}
	  
			/* 
			 * Frete
			 */
			
			$this->load->model('localisation/zone');
			
			if($this->config->get('payment_pagseguro_envia_endereco_entrega')) {
				$paymentRequest->addParameter('shippingAddressRequired', 'true');	
				
				$tipo_frete = $this->config->get('payment_pagseguro_tipo_frete');
				
				if($tipo_frete){
					$paymentRequest->setShippingType($tipo_frete);	    	
				}
				else{
					$paymentRequest->setShippingType(3); // 3: Não especificado
				}				
				
				if ($this->cart->hasShipping()) {
					
					$number = '';
					
					if($this->config->get('payment_pagseguro_campo_numero') != "" && isset($order_info['shipping_' . $this->config->get('payment_pagseguro_campo_numero')]) && $order_info['shipping_' . $this->config->get('payment_pagseguro_campo_numero')] != ''){
						$number = html_entity_decode($order_info['shipping_' . $this->config->get('payment_pagseguro_campo_numero')], ENT_QUOTES, 'UTF-8');
					} else if($this->config->get('payment_pagseguro_campo_numero') != ""){
						$numero = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_numero'));
						
						if($numero){
							$number = html_entity_decode($numero, ENT_QUOTES, 'UTF-8');
						}
					}

					$complement = '';
					
					if($this->config->get('payment_pagseguro_campo_complemento') != "" && isset($order_info['shipping_' . $this->config->get('payment_pagseguro_campo_complemento')]) && $order_info['shipping_' . $this->config->get('payment_pagseguro_campo_complemento')] != ''){
						$complement = html_entity_decode($order_info['shipping_' . $this->config->get('payment_pagseguro_campo_complemento')], ENT_QUOTES, 'UTF-8');
					} else if($this->config->get('payment_pagseguro_campo_complemento') != ""){
						$complemento = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_complemento'));
						
						if($complemento){
							$complement = html_entity_decode($complemento, ENT_QUOTES, 'UTF-8');
						}
					}			

					$zone = $this->model_localisation_zone->getZone($order_info['shipping_zone_id']);
					
					// Endereço para entrega		
					$paymentRequest->setShippingAddress(  
						Array(  
							'postalCode'=> preg_replace ("/[^0-9]/", '', $order_info['shipping_postcode']),
							'street' 	=> $this->aparar($order_info['shipping_address_1'], 80),
							'number' 	=> $this->aparar($number, 20),
							'complement'=> $this->aparar($complement, 40),
							'district' 	=> $this->aparar($order_info['shipping_address_2'], 60),      
							'city' 		=> $this->aparar($order_info['shipping_city'], 60),        
							'state' 	=> (isset($zone['code'])) ? $zone['code'] : '',       
							'country' 	=> $order_info['shipping_iso_code_3']
						)  
					);
				}
				else{
					$number = '';
					
					if($this->config->get('payment_pagseguro_campo_numero') != "" && isset($order_info['payment_' . $this->config->get('payment_pagseguro_campo_numero')]) && $order_info['payment_' . $this->config->get('payment_pagseguro_campo_numero')] != ''){
						$number = html_entity_decode($order_info['payment_' . $this->config->get('payment_pagseguro_campo_numero')], ENT_QUOTES, 'UTF-8');
					} else if($this->config->get('payment_pagseguro_campo_numero') != ""){
						$numero = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_numero'));
						
						if($numero){
							$number = html_entity_decode($numero, ENT_QUOTES, 'UTF-8');
						}
					}

					$complement = '';
					
					if($this->config->get('payment_pagseguro_campo_complemento') != "" && isset($order_info['payment_' . $this->config->get('payment_pagseguro_campo_complemento')]) && $order_info['payment_' . $this->config->get('payment_pagseguro_campo_complemento')] != ''){
						$complement = html_entity_decode($order_info['payment_' . $this->config->get('payment_pagseguro_campo_complemento')], ENT_QUOTES, 'UTF-8');
					} else if($this->config->get('payment_pagseguro_campo_complemento') != ""){
						$complemento = $this->model_extension_payment_pagseguro->getFieldValue($order_info, $custom_fields, $this->config->get('payment_pagseguro_campo_complemento'));
						
						if($complemento){
							$complement = html_entity_decode($complemento, ENT_QUOTES, 'UTF-8');
						}
					}			
					
					$zone = $this->model_localisation_zone->getZone($order_info['payment_zone_id']);
					
					// Endereço para entrega		
					$paymentRequest->setShippingAddress(  
						Array(  
							'postalCode'=> preg_replace ("/[^0-9]/", '', $order_info['payment_postcode']),
							'street' 	=> $this->aparar($order_info['payment_address_1'], 80),     
							'number' 	=> $this->aparar($number, 20),
							'complement'=> $this->aparar($complement, 40),
							'district' 	=> $this->aparar($order_info['payment_address_2'], 60),       
							'city' 		=> $this->aparar($order_info['payment_city'], 60),          
							'state' 	=> (isset($zone['code'])) ? $zone['code'] : '',       
							'country' 	=> $order_info['payment_iso_code_3']
						)  
					);			
				}	   	
			} else {
				$paymentRequest->addParameter('shippingAddressRequired', 'false');
			}
			/*
			 * Produtos
			 */
			$this->load->model('tool/upload');
			
			foreach ($this->cart->getProducts() as $product) {
				$options_names = array();
				$model = ($product['model'] != '') ? ' | Modelo: ' . $product['model'] : '';
				
				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}
					$options_names[] = $option['name'] . ": " . (strlen(utf8_decode($value)) > 20 ? utf8_decode(substr($value, 0, 20)) . '..' : $value);
				}
					
				if(!empty($options_names)){
					$options = " | Opções:: " . implode(', ', $options_names);
				}
				else{
					$options = '';
				}
				
				// limite de 100 caracteres para a descrição do produto
				$description = $product['name'] . $model . $options;
				$description = $this->aparar($description, 100);
				
				$amount = $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value'], false);
				
				$item = Array(
					'id' => $product['product_id'],
					'description' => trim($description),
					'quantity' => $product['quantity'],
					'amount' => $amount
				);
				
				// O frete será calculado pelo PagSeguro.
				if($this->config->get('payment_pagseguro_envia_endereco_entrega') && $tipo_frete){
					$peso = $this->getPesoEmGramas($product['weight_class_id'], $product['weight'])/$product['quantity'];
					$item['weight'] = round($peso);
				}

				$paymentRequest->addItem($item);
			}
			
			/*
			 * Parâmetros
			 */
			 
			$indice = 1;
			
			$grupos_pagamento_aceito = array();
			$grupos_pagamento_excluido = array();
			
			if($this->config->get('payment_pagseguro_boleto_status')) {
				$grupos_pagamento_aceito[] = 'BOLETO';
				$sub_indice = 1;				
				$desconto_boleto = (float)$this->config->get('payment_pagseguro_boleto_desconto');
				
				if($desconto_boleto > 0 && $desconto_boleto < 100) {
					$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'BOLETO');
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'DISCOUNT_PERCENT');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, number_format($this->config->get('payment_pagseguro_boleto_desconto'), 2));
					
					$indice++;
				}
			} else {
				$grupos_pagamento_excluido[] = 'BOLETO';
			}
			
			if($this->config->get('payment_pagseguro_cartao_credito_status')) {
				$grupos_pagamento_aceito[] = 'CREDIT_CARD';
				$sub_indice = 1;	
				$add_group = false;
				$desconto_cartao = (float)$this->config->get('payment_pagseguro_cartao_credito_desconto');
				
				if($desconto_cartao > 0 && $desconto_cartao < 100) {
					$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'CREDIT_CARD');
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'DISCOUNT_PERCENT');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, number_format($this->config->get('payment_pagseguro_cartao_credito_desconto'), 2));
					$add_group = true;
					$sub_indice++;
				}
				/*
				$max_parcelas = (int)$this->config->get('payment_pagseguro_cartao_credito_qtde_max_parcelas');
				
				if($max_parcelas > 0 && $max_parcelas < 18) {
					if(!$add_group) {
						$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'CREDIT_CARD');
						$add_group = true;
					}
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'MAX_INSTALLMENTS_LIMIT');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, $max_parcelas);
					
					$sub_indice++;
				}
				*/
				$parcelas_sem_juros = (int)$this->config->get('payment_pagseguro_cartao_credito_qtde_parcelas_sem_juros');
				
				if($parcelas_sem_juros > 1 && $parcelas_sem_juros <= 18) {
					if(!$add_group) {
						$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'CREDIT_CARD');
						$add_group = true;
					}
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'MAX_INSTALLMENTS_NO_INTEREST');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, $parcelas_sem_juros);
					
					$sub_indice++;
				}

				if($add_group) {
					$indice++;
				}
			} else {
				$grupos_pagamento_excluido[] = 'CREDIT_CARD';
			}

			if($this->config->get('payment_pagseguro_deposito_status')) {
				$grupos_pagamento_aceito[] = 'DEPOSIT';
				$sub_indice = 1;	
				$desconto_deposito = (float)$this->config->get('payment_pagseguro_deposito_desconto');
				
				if($desconto_deposito > 0 && $desconto_deposito < 100) {
					$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'DEPOSIT');
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'DISCOUNT_PERCENT');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, number_format($this->config->get('payment_pagseguro_deposito_desconto'), 2));
					
					$indice++;
					$sub_indice++;
				}				
			} else {
				$grupos_pagamento_excluido[] = 'DEPOSIT';
			}

			if($this->config->get('payment_pagseguro_debito_online_status')) {
				$grupos_pagamento_aceito[] = 'EFT';
				$sub_indice = 1;	
				$desconto_debito = (float)$this->config->get('payment_pagseguro_debito_online_desconto');
				
				if($desconto_debito > 0 && $desconto_debito < 100) {
					$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'EFT');
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'DISCOUNT_PERCENT');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, number_format($this->config->get('payment_pagseguro_debito_online_desconto'), 2));
					
					$indice++;
					$sub_indice++;
				}				
			} else {
				$grupos_pagamento_excluido[] = 'EFT';
			}	

			if($this->config->get('payment_pagseguro_saldo_pagseguro_status')) {
				$grupos_pagamento_aceito[] = 'BALANCE';
				$sub_indice = 1;	
				$desconto_saldo = (float)$this->config->get('payment_pagseguro_saldo_pagseguro_desconto');
				
				if($desconto_saldo > 0 && $desconto_saldo < 100) {
					$paymentRequest->addParameter('paymentMethodGroup' . $indice, 'BALANCE');
					$paymentRequest->addParameter('paymentMethodConfigKey' . $indice . '_' . $sub_indice, 'DISCOUNT_PERCENT');
					$paymentRequest->addParameter('paymentMethodConfigValue' . $indice . '_' . $sub_indice, number_format($this->config->get('payment_pagseguro_saldo_pagseguro_desconto'), 2));
					
					$indice++;
					$sub_indice++;
				}				
			} else {
				$grupos_pagamento_excluido[] = 'BALANCE';
			}			
			
			if(!empty($grupos_pagamento_aceito)) {
				$paymentRequest->addParameter('acceptPaymentMethodGroup', implode(',', $grupos_pagamento_aceito));
			}
			
			if(!empty($grupos_pagamento_excluido)) {
				$paymentRequest->addParameter('excludePaymentMethodGroup', implode(',', $grupos_pagamento_excluido));
			}
			
			// Referência do pedido no PagSeguro
			if($this->config->get('payment_pagseguro_posfixo') != ""){
				$paymentRequest->setReference($this->session->data['order_id']."_".$this->config->get('payment_pagseguro_posfixo'));
			}
			else{
				$paymentRequest->setReference($this->session->data['order_id']);
			}	    

			// url para redirecionar o comprador ao finalizar o pagamento
			$paymentRequest->setRedirectUrl($this->url->link('checkout/success'));
			
			// url para receber notificações sobre o status das transações
			$paymentRequest->setNotificationURL($this->url->link('extension/payment/pagseguro/callback', '', true)); 
			
			// obtendo frete, descontos e taxas
			if(isset($this->session->data['shipping_method']['cost']) && (float)$this->session->data['shipping_method']['cost']) {
				$shipping_cost = $this->session->data['shipping_method']['cost'];
				
				if($this->config->get('payment_pagseguro_envia_endereco_entrega')) {
					$paymentRequest->setShippingCost($this->currency->format($shipping_cost, $order_info['currency_code'], $order_info['currency_value'], false));
				} else {
					$item = Array(
						'id' 			=> 'frete',
						'description' 	=> $this->aparar($this->session->data['shipping_method']['title'], 100),
						'quantity' 		=> 1,
						'amount' 		=> $this->currency->format($shipping_cost, $order_info['currency_code'], $order_info['currency_value'], false)
					);
					
					$paymentRequest->addItem($item);			
				}				
				
				$extra = $this->currency->format($order_info['total'] - $this->cart->getSubTotal() - $shipping_cost, $order_info['currency_code'], $order_info['currency_value'], false);

				if ($extra != 0) {
					$paymentRequest->setExtraAmount($extra);			
				}				
			} else {
				$extra = $this->currency->format($order_info['total'] - $this->cart->getSubTotal(), $order_info['currency_code'], $order_info['currency_value'], false);
				
				if ($extra != 0) {
					$paymentRequest->setExtraAmount($extra);			
				}				
			}			
			
			/* 
			 * Fazendo a chamada para a API de Pagamentos do PagSeguro. 
			 * Se tiver sucesso, retorna o código (url) de requisição para este pagamento.
			 */
			$data['url'] = '';
			$data['api_code'] = '';
			$data['pagseguro_exception'] = '';
			try {
				if($this->config->get('payment_pagseguro_ambiente') == 'sandbox') {
					$token = $this->config->get('payment_pagseguro_token_sandbox');
				} else {
					$token = $this->config->get('payment_pagseguro_token');
				}
				
				$credentials = new PagSeguroAccountCredentials(trim($this->config->get('payment_pagseguro_email')), trim($token));
				$url = $paymentRequest->register($credentials);
				$data['url'] = $url;
				
				$api_code = explode('code=', $url);
				$data['api_code'] = $api_code[1];

			} catch (PagSeguroServiceException $e) {
				$oneline_message = $e->getOneLineMessage();
				$this->log->write('PagSeguro :: ' . $oneline_message);
				$data['pagseguro_exception'] = $oneline_message;
			}

			$data['payment_pagseguro_ambiente'] = $this->config->get('payment_pagseguro_ambiente');
		
			return $this->load->view('extension/payment/pagseguro', $data);
		}
	}
		
	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'pagseguro') {
			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('config_order_status_id'));
		}
	}
			
	public function callback() {
		
		require_once(DIR_SYSTEM . 'library/PagSeguroLibrary/PagSeguroLibrary.php');
		
		if($this->config->get('payment_pagseguro_ambiente') == 'sandbox') {
			PagSeguroConfig::setEnvironment('sandbox');
			$token = $this->config->get('payment_pagseguro_token_sandbox');
		} else {
			$token = $this->config->get('payment_pagseguro_token');
		}		
		
		$code = (isset($_POST['notificationCode']) && trim($_POST['notificationCode']) != "") ? trim($_POST['notificationCode']) : null;
    	$type = (isset($_POST['notificationType']) && trim($_POST['notificationType']) != "") ? trim($_POST['notificationType']) : null;
    	
    	if($code && $type) {

    		$notificationType = new PagSeguroNotificationType($type);
    		$strType = $notificationType->getTypeFromValue();
    		
    		switch($strType) {
				
				case 'TRANSACTION':
					
    				$credentials = new PagSeguroAccountCredentials(trim($this->config->get('payment_pagseguro_email')), trim($token));
										
    		    	try {
			    		$transaction = PagSeguroNotificationService::checkTransaction($credentials, $code);
			    		
			    		$transactionStatus	= $transaction->getStatus();
			    		$id_pedido 			= explode('_', $transaction->getReference());
			    		$paymentMethod 		= $transaction->getPaymentMethod();
			    		$parcelas 			= $transaction->getInstallmentCount();
						$pagSeguroShipping 	= $transaction->getShipping();
						$codigo_transacao	= $transaction->getCode(); 
						  
						$this->load->model('checkout/order');
						$order = $this->model_checkout_order->getOrder($id_pedido[0]);
						
						// Obtendo o tipo de pagamento escolhido
						$payment_code = $paymentMethod->getCode();
    		    		$comment = "Código da transação: " . $codigo_transacao . "\nTipo de pagamento: " . $payment_code->getTypeFromValue()."\nParcelas: ".$parcelas . "\n";
 
						// checando se houve desconto durante o pagamento
						$valor_desconto		= $transaction->getDiscountAmount();
						
						if((float)$valor_desconto) {
							$valor_bruto		= $this->currency->format($transaction->getGrossAmount(), $order['currency_code'], $order['currency_value'], false); 
							$valor_desconto		= $this->currency->format($valor_desconto, $order['currency_code'], $order['currency_value'], false);
							
							$comment .= "\nTotal: " . $valor_bruto . "\nDesconto: " . $valor_desconto;
							/*
							$query_order_total = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$id_pedido[0] . "' AND `code` = 'pagseguro_desconto_jc757'");
							
							if($query_order_total->num_rows) {
								$this->db->query("UPDATE " . DB_PREFIX . "order_total SET `code` = 'pagseguro_desconto_jc757', title = 'Desconto PagSeguro', `value` = '" . ($valor_desconto * -1) . "', sort_order = '8' WHERE order_id = '" . (int)$id_pedido[0] . "'");
							} else {
								$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$id_pedido[0] . "', `code` = 'pagseguro_desconto_jc757', title = 'Desconto PagSeguro', `value` = '" . ($valor_desconto * -1) . "', sort_order = '8'");
							}*/
						}
 
    		    		// Obtendo o tipo e o valor do frete
						if(isset($pagSeguroShipping) && $pagSeguroShipping instanceof PagSeguroShipping) {
							$pagSeguroShippingType = $pagSeguroShipping->getType(); 
							$valor_frete = $pagSeguroShipping->getCost();
							
							// Valor 1: Pac, valor 2: Sedex, valor 3: não especificado ou cálculo não realizado pelo PagSeguro
							if($pagSeguroShippingType->getValue() != 3 && (float)$valor_frete) {
								$tipo_frete = $pagSeguroShippingType->getTypeFromValue();
								
								$comment .= "\nTipo de frete escolhido no PagSeguro: " . $tipo_frete . "\nValor do frete: " . $this->currency->format($valor_frete, $order['currency_code'], $order['currency_value'], false);
								/*
								$query_order_total = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$id_pedido[0] . "' AND `code` = 'pagseguro_frete_jc757'");
								
								if($query_order_total->num_rows) {
									$this->db->query("UPDATE " . DB_PREFIX . "order_total SET `code` = 'pagseguro_frete_jc757', title = '" . $this->db->escape($tipo_frete) . "', `value` = '" . (float)$valor_frete . "', sort_order = '3' WHERE order_id = '" . (int)$id_pedido[0] . "'");
								} else {
									$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$id_pedido[0] . "', `code` = 'pagseguro_frete_jc757', title = '" . $this->db->escape($tipo_frete) . "', `value` = '" . (float)$valor_frete . "', sort_order = '3'");
								}*/							
							}
						}
	    
					    $update_status_alert = false;
					    if($this->config->get('payment_pagseguro_update_status_alert')){
					    	$update_status_alert = true;
					    }
					    
						switch($transactionStatus->getTypeFromValue()){
				
							case 'WAITING_PAYMENT':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_aguardando_pagamento')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_aguardando_pagamento'), $comment, $update_status_alert);
								}
								break;
											
							case 'IN_ANALYSIS':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_analise')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_analise'), $comment, $update_status_alert);
								}
								break;
							
							case 'PAID':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_paga')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_paga'), $comment, $update_status_alert);
								}
								break;
							case 'AVAILABLE':
								//if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_disponivel')){
								//	$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_disponivel'), '', false);
								//}
								break;
							case 'IN_DISPUTE':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_disputa')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_disputa'), $comment, $update_status_alert);
								}
								break;
							case 'REFUNDED':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_devolvida')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_devolvida'), $comment, $update_status_alert);
								}
								break;
							case 'CANCELLED':
								$cancellationSource = $transaction->getCancellationSource();
								$origem = $cancellationSource->getTypeFromValue($cancellationSource->getValue());
								
								$comment .= "\nCancelado por: " . $origem;
								
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_cancelada')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_cancelada'), $comment, $update_status_alert);
								}
								break;	
							case 'CONTESTATION':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_retencao_temporaria')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_retencao_temporaria'), $comment, $update_status_alert);
								}
								break;
							case 'SELLER_CHARGEBACK':
								if($order['order_status_id'] != $this->config->get('payment_pagseguro_order_debitada')){
									$this->model_checkout_order->addOrderHistory($id_pedido[0], $this->config->get('payment_pagseguro_order_debitada'), $comment, $update_status_alert);
								}
								break;									
							default:
								$this->model_checkout_order->addOrderHistory($id_pedido[0], $order['order_status_id'], $comment, false);									
						}						
			    		
			    	} catch (PagSeguroServiceException $e) {
						$this->log->write('PagSeguro :: ' . $e->getOneLineMessage());
			    	}					
					break;
				
				default:
					$this->log->write('PagSeguro :: tipo de notificação desconhecido ['.$notificationType->getValue().']');
			}    		
    	}
    	else{
    		$this->log->write('PagSeguro :: Parâmetros de notificação (notificationCode e notificationType) retornados vazios pelo PagSeguro. 1) Verifique se o link de \'Notificação da Transação no site do PagSeguro\' está configurado corretamente. 2) Clique na transação (no site do PagSeguro) para abrir os detalhes. Clique no link \'Notificações da transação enviadas para o servidor\' para mais detalhes do erro.');
    	}	
	}
	
	private function getPesoEmGramas($weight_class_id, $peso){
		
		if($this->weight->getUnit($weight_class_id) == 'g'){
			return $peso;
		}
		return $peso * 1000;
	}
	
	private function isCPF($str) {
		if (!preg_match('|^(\d{3})\.?(\d{3})\.?(\d{3})\-?(\d{2})$|', $str, $matches)){
			return false;
		}

		array_shift($matches);
		$str = implode('', $matches);

		for ($i=0; $i < 10; $i++){
			if ($str == str_repeat($i, 11)){
				return false;
			}
		}

		for ($t=9; $t < 11; $t++) {
			for ($d=0, $c=0; $c < $t; $c++){
				$d += $str[$c] * ($t + 1 - $c);
			}
		
			$d = ((10 * $d) % 11) % 10;
		
			if ($str[$c] != $d){
				return false;
			}
		}

		return $str;
	}

	private function isCNPJ($cnpj) {
		$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
		// Valida tamanho
		if (strlen($cnpj) != 14)
			return false;
		// Valida primeiro dígito verificador
		for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
		{
			$soma += $cnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}
		$resto = $soma % 11;
		if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto))
			return false;
		// Valida segundo dígito verificador
		for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
		{
			$soma += $cnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}
		$resto = $soma % 11;
		return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);
	}	

	private function aparar($string, $limite){
		
		$mb_substr = (function_exists("mb_substr")) ? true : false;	
		
		if($mb_substr){
			return mb_substr($string, 0, $limite, 'UTF-8');
		}
		else{
			return utf8_encode(substr(utf8_decode($string), 0, $limite));
		}		
	}	
}

