<?php
// Heading
$_['heading_title']       			= 'PagSeguro Lightbox Pro';

// Text
$_['text_extension']     			= 'Extensões';
$_['text_edit']                 	= 'Editar o PagSeguro';
$_['text_payment']        			= 'Pagamento';
$_['text_success']        			= 'Sucesso: Você modificou os detalhes da conta do PagSeguro!';
$_['text_pagseguro']				= '<a target="_BLANK" href="https://www.pagseguro.com.br"><img src="view/image/payment/pagseguro_uol.gif" alt="PagSeguro" title="PagSeguro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_frete_loja']        		= 'pela loja';
$_['text_frete_pagseguro_pac']      = 'pelo PagSeguro usando PAC';
$_['text_frete_pagseguro_sedex']    = 'pelo PagSeguro usando Sedex';
$_['text_frete_pagseguro_nao_especificado'] = 'pelo PagSeguro. O cliente escolhe entre PAC e Sedex';
$_['text_sandbox']                 	= 'Teste (sandbox)';
$_['text_producao']                 = 'Produção';
$_['text_default_name'] 			= 'Cartão/Depósito/Boleto (via PagSeguro)';
$_['text_dados_da_conta'] 			= 'Dados da Conta';
$_['text_opcoes_adicionais'] 		= 'Opções Adicionais';
$_['text_opcoes_entrega'] 			= 'Opções de Frete e Entrega';
$_['text_campos_personalizados'] 	= 'Campos Adicionais (Personalizados)';
$_['text_situacoes_pedido'] 		= 'Situações do Pedido';
$_['text_geral'] 					= 'Geral';

// Entry
$_['entry_token']         				= 'Token de produção';
$_['entry_token_sandbox']         		= 'Token de testes (sandbox)';
$_['entry_email']         				= 'Email';
$_['entry_posfixo']         			= 'Pós-fixo do número do pedido';
$_['entry_tipo_frete']         			= 'Cálculo do frete feito';
$_['entry_order_aguardando_pagamento'] 	= 'Aguardando pagamento';
$_['entry_order_analise'] 				= 'Em análise';
$_['entry_order_paga'] 					= 'Paga';
$_['entry_order_disputa'] 				= 'Disputa';
$_['entry_order_devolvida'] 			= 'Devolvida';
$_['entry_order_cancelada'] 			= 'Cancelada';
$_['entry_order_retencao_temporaria'] 	= 'Pagamento contestado';
$_['entry_order_debitada'] 				= 'Reembolsado pelo vendedor';
$_['entry_total']       			 	= 'Total Mínimo'; 
$_['entry_geo_zone']      				= 'Região geográfica';
$_['entry_status']        				= 'Situação';
$_['entry_sort_order']    				= 'Ordem';
$_['entry_update_status_alert'] 		= 'Alertar sobre mudança no status';
$_['entry_campo_complemento']  			= 'Campo do Complemento do endereço';  
$_['entry_campo_numero']  				= 'Campo do Número do endereço'; 
$_['entry_campo_ddd']  					= 'Campo do DDD'; 
$_['entry_campo_celular']  				= 'Campo do Celular'; 
$_['entry_campo_cpf']  					= 'Campo do CPF';  
$_['entry_campo_cnpj']  				= 'Campo do CNPJ';  
$_['entry_ambiente']  					= 'Ambiente'; 
$_['entry_name']  						= 'Nome'; 
$_['entry_habilitado']        		= 'Habilitar?';
$_['entry_desconto']        		= 'Desconto (%)';
$_['entry_qtde_max_parcelas']       = 'Parcelamento máximo';
$_['entry_qtde_parcelas_sem_juros'] = 'Parcelas sem juros';
 $_['entry_envia_endereco_entrega']  = 'Requer endereço de entrega';

// Tabs
$_['tab_config_geral']    			= 'Configuração Geral';
$_['tab_config_boleto']    			= 'Boleto';
$_['tab_config_cartao_credito']    	= 'Cartão de Crédito';
$_['tab_config_deposito']    		= 'Depósito';
$_['tab_config_debito_online']    	= 'Débito Online';
$_['tab_config_saldo_pagseguro']    = 'Saldo PagSeguro';

// Help
$_['help_habilitado']    			= 'Habilita ou desabilita esse meio de pagamento';
$_['help_desconto']  				= 'Valor maior que 0.00 e menor que 100.00. Use ponto para separar os decimais';
$_['help_qtde_max_parcelas']  		= 'Aceita um valor entre 1 e 18';
$_['help_qtde_parcelas_sem_juros']  = 'Aceita um valor entre 2 e 18';
$_['help_token']				= 'Token de Segurança fornecido pelo PagSeguro (ambiente de produção)';
$_['help_token_sandbox']		= 'Token de Segurança fornecido pelo PagSeguro (ambiente de testes)';
$_['help_email']				= 'Email da conta no PagSeguro';
$_['help_posfixo']				= 'Útil para identificar no PagSeguro de qual loja pertence o pedido. Ex. para pedido de nro. 15 e pós-fixo lojaX, a referência do pedido no PagSeguro será 15_lojaX';
$_['help_tipo_frete']			= 'Se optar pelo cálculo feito pela loja, escolha Frete Fixo em Preferências de frete no PagSeguro senão marque Frete Por Peso para que o PagSeguro faça os cálculos.';
$_['help_order_aguardando_pagamento']	= 'O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.';
$_['help_order_analise']		= 'O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.';
$_['help_order_paga']			= 'A transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.';
$_['help_order_disputa']		= 'O comprador, dentro do prazo de liberação da transação, abriu uma disputa.';
$_['help_order_devolvida']		= 'O valor da transação foi devolvido para o comprador.';
$_['help_order_cancelada']		= 'A transação foi cancelada sem ter sido finalizada.';
$_['help_order_retencao_temporaria'] 	= 'O pagamento foi contestado pelo comprador e o valor da transação foi bloqueado.';
$_['help_order_debitada'] 				= 'O pagamento contestado pelo comprador e que teve o valor da transação bloqueado anteriormente, foi devolvido ao comprador.';
$_['help_update_status_alert']	= 'Envia email para o cliente avisando-o sobre mudança no status do pedido.';
$_['help_total']				= 'Total mínimo que o pedido deve alcançar para que este método de pagamento seja habilitado.';
$_['help_campo_complemento']	= 'O nome do campo personalizado para o Complemento do endereço.';
$_['help_campo_numero']			= 'O nome do campo personalizado para o Número.';
$_['help_campo_ddd']			= 'O nome do campo personalizado para o DDD do celular. Use somente se tem um campo para o DDD separado do Celular';
$_['help_campo_celular']		= 'O nome do campo personalizado para o Celular. O módulo usará o campo Telefone nativo do OpenCart se deixar essa opção vazia.';
$_['help_campo_cpf']			= 'O nome do campo personalizado para o CPF.';
$_['help_campo_cnpj']			= 'O nome do campo personalizado para o CNPJ.';
$_['help_ambiente']				= 'Teste sua integração de pagamento sem alterar as transações reais escolhendo o ambiente de testes (sandbox). Para transações reais, escolha ambiente de produção';
$_['help_name']					= 'É o nome que aparecerá para o cliente nas opções de pagamentos. Você também pode usar uma imagem desde que esteja com a tag img.';
$_['help_campos_personalizados']= 'Se sua loja possui campos personalizados, relacione-os abaixo pra enviar esses dados para o PagSeguro. Esses campos são cadastrados <a href="index.php?route=customer/custom_field&user_token=%s">aqui</a>';
$_['help_opcoes_entrega']		=  'Cálculo do frete feito pelo PagSeguro é muito especial e pra ativá-lo você tem: 1) Desabilitar todos os módulos de Frete em sua loja. 2) Desabilitar a opção/campo *Requer frete* em cada produto. 3) Escolher Frete Por Peso em Preferências de frete no PagSeguro. Recomendamos deixar o cálculo feito pela Loja, a não se que saiba o que está fazendo.';
 $_['help_envia_endereco_entrega'] 	= 'Habilita o envio do endereço de entrega do cliente para o PagSeguro. Se o cálculo do frete não é feito pelo PagSeguro, deixe desabilitado. Se o cálculo de frete é feito pelo PagSeguro, deixe habilitado.';
$_['help_situacoes_pedido'] 		= 'Aqui você relaciona a situação (ou status) do pedido no PagSeguro (esquerda) com a situação do pedido em sua loja (direita). O status é alterado automaticamente em sua loja quando o mesmo é alterado no PagSeguro. Não é necessário configurar a URL de Retorno pois a mesma é enviada automaticamente pelo módulo para o PagSeguro.';

// Error
$_['error_permission']    		= 'Atenção: Você não possui permissão para modificar o PagSeguro!';
$_['error_token']         		= 'Digite o token de segurança';
$_['error_email']         		= 'Digite o e-mail de cadastro no PagSeguro';
 