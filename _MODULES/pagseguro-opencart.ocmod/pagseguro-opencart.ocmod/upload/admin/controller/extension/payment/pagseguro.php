<?php

class ControllerExtensionPaymentPagseguro extends Controller
{
  private $error = array();

  public function index()
  {
    $this->load->language('extension/payment/pagseguro');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('setting/setting');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
      $this->model_setting_setting->editSetting('payment_pagseguro', $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
    }

    $data['pagseguro_version'] = 'v2.8 - 04/09/2019';

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['token'])) {
      $data['error_token'] = $this->error['token'];
    } else {
      $data['error_token'] = '';
    }

    if (isset($this->error['email'])) {
      $data['error_email'] = $this->error['email'];
    } else {
      $data['error_email'] = '';
    }
/* 
    if (isset($this->error['chave'])) {
      $data['error_chave'] = $this->error['chave'];
    } else {
      $data['error_chave'] = '';
    } */

    if (isset($this->error['result'])) {
      $data['error_result'] = $this->error['result'];
    } else {
      $data['error_result'] = '';
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_extension'),
      'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('extension/payment/pagseguro', 'user_token=' . $this->session->data['user_token'], true)
    );

    $data['action'] = $this->url->link('extension/payment/pagseguro', 'user_token=' . $this->session->data['user_token'], true);

    $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

    if (isset($this->request->post['payment_pagseguro_ambiente'])) {
      $data['payment_pagseguro_ambiente'] = $this->request->post['payment_pagseguro_ambiente'];
    } else {
      $data['payment_pagseguro_ambiente'] = $this->config->get('payment_pagseguro_ambiente');
    }

    if (isset($this->request->post['payment_pagseguro_token'])) {
      $data['payment_pagseguro_token'] = $this->request->post['payment_pagseguro_token'];
    } else {
      $data['payment_pagseguro_token'] = $this->config->get('payment_pagseguro_token');
    }

    if (isset($this->request->post['payment_pagseguro_token_sandbox'])) {
      $data['payment_pagseguro_token_sandbox'] = $this->request->post['payment_pagseguro_token_sandbox'];
    } else {
      $data['payment_pagseguro_token_sandbox'] = $this->config->get('payment_pagseguro_token_sandbox');
    }

    if (isset($this->request->post['payment_pagseguro_email'])) {
      $data['payment_pagseguro_email'] = $this->request->post['payment_pagseguro_email'];
    } else {
      $data['payment_pagseguro_email'] = $this->config->get('payment_pagseguro_email');
    }

    if (isset($this->request->post['payment_pagseguro_posfixo'])) {
      $data['payment_pagseguro_posfixo'] = $this->request->post['payment_pagseguro_posfixo'];
    } else {
      $data['payment_pagseguro_posfixo'] = $this->config->get('payment_pagseguro_posfixo');
    }

    if (isset($this->request->post['payment_pagseguro_total'])) {
      $data['payment_pagseguro_total'] = $this->request->post['payment_pagseguro_total'];
    } else {
      $data['payment_pagseguro_total'] = $this->config->get('payment_pagseguro_total');
    }

    if (isset($this->request->post['payment_pagseguro_tipo_frete'])) {
      $data['payment_pagseguro_tipo_frete'] = $this->request->post['payment_pagseguro_tipo_frete'];
    } else {
      $data['payment_pagseguro_tipo_frete'] = $this->config->get('payment_pagseguro_tipo_frete');
    }

    if (isset($this->request->post['payment_pagseguro_update_status_alert'])) {
      $data['payment_pagseguro_update_status_alert'] = $this->request->post['payment_pagseguro_update_status_alert'];
    } else {
      $data['payment_pagseguro_update_status_alert'] = $this->config->get('payment_pagseguro_update_status_alert');
    }

    if (isset($this->request->post['payment_pagseguro_order_aguardando_retorno'])) {
      $data['payment_pagseguro_order_aguardando_retorno'] = $this->request->post['payment_pagseguro_order_aguardando_retorno'];
    } else {
      $data['payment_pagseguro_order_aguardando_retorno'] = $this->config->get('payment_pagseguro_order_aguardando_retorno');
    }

    if (isset($this->request->post['payment_pagseguro_order_aguardando_pagamento'])) {
      $data['payment_pagseguro_order_aguardando_pagamento'] = $this->request->post['payment_pagseguro_order_aguardando_pagamento'];
    } else {
      $data['payment_pagseguro_order_aguardando_pagamento'] = $this->config->get('payment_pagseguro_order_aguardando_pagamento');
    }

    if (isset($this->request->post['payment_pagseguro_order_analise'])) {
      $data['payment_pagseguro_order_analise'] = $this->request->post['payment_pagseguro_order_analise'];
    } else {
      $data['payment_pagseguro_order_analise'] = $this->config->get('payment_pagseguro_order_analise');
    }

    if (isset($this->request->post['payment_pagseguro_order_paga'])) {
      $data['payment_pagseguro_order_paga'] = $this->request->post['payment_pagseguro_order_paga'];
    } else {
      $data['payment_pagseguro_order_paga'] = $this->config->get('payment_pagseguro_order_paga');
    }

    if (isset($this->request->post['payment_pagseguro_order_disponivel'])) {
      $data['payment_pagseguro_order_disponivel'] = $this->request->post['payment_pagseguro_order_disponivel'];
    } else {
      $data['payment_pagseguro_order_disponivel'] = $this->config->get('payment_pagseguro_order_disponivel');
    }

    if (isset($this->request->post['payment_pagseguro_order_disputa'])) {
      $data['payment_pagseguro_order_disputa'] = $this->request->post['payment_pagseguro_order_disputa'];
    } else {
      $data['payment_pagseguro_order_disputa'] = $this->config->get('payment_pagseguro_order_disputa');
    }

    if (isset($this->request->post['payment_pagseguro_order_devolvida'])) {
      $data['payment_pagseguro_order_devolvida'] = $this->request->post['payment_pagseguro_order_devolvida'];
    } else {
      $data['payment_pagseguro_order_devolvida'] = $this->config->get('payment_pagseguro_order_devolvida');
    }

    if (isset($this->request->post['payment_pagseguro_order_cancelada'])) {
      $data['payment_pagseguro_order_cancelada'] = $this->request->post['payment_pagseguro_order_cancelada'];
    } else {
      $data['payment_pagseguro_order_cancelada'] = $this->config->get('payment_pagseguro_order_cancelada');
    }

    if (isset($this->request->post['payment_pagseguro_order_debitada'])) {
      $data['payment_pagseguro_order_debitada'] = $this->request->post['payment_pagseguro_order_debitada'];
    } else {
      $data['payment_pagseguro_order_debitada'] = $this->config->get('payment_pagseguro_order_debitada');
    }

    if (isset($this->request->post['payment_pagseguro_order_retencao_temporaria'])) {
      $data['payment_pagseguro_order_retencao_temporaria'] = $this->request->post['payment_pagseguro_order_retencao_temporaria'];
    } else {
      $data['payment_pagseguro_order_retencao_temporaria'] = $this->config->get('payment_pagseguro_order_retencao_temporaria');
    }

    $this->load->model('localisation/order_status');

    $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

    if (isset($this->request->post['payment_pagseguro_geo_zone_id'])) {
      $data['payment_pagseguro_geo_zone_id'] = $this->request->post['payment_pagseguro_geo_zone_id'];
    } else {
      $data['payment_pagseguro_geo_zone_id'] = $this->config->get('payment_pagseguro_geo_zone_id');
    }

    $this->load->model('localisation/geo_zone');

    $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

    if (isset($this->request->post['payment_pagseguro_status'])) {
      $data['payment_pagseguro_status'] = $this->request->post['payment_pagseguro_status'];
    } else {
      $data['payment_pagseguro_status'] = $this->config->get('payment_pagseguro_status');
    }

    if (isset($this->request->post['payment_pagseguro_sort_order'])) {
      $data['payment_pagseguro_sort_order'] = $this->request->post['payment_pagseguro_sort_order'];
    } else {
      $data['payment_pagseguro_sort_order'] = $this->config->get('payment_pagseguro_sort_order');
    }

    if (isset($this->request->post['payment_pagseguro_campo_numero'])) {
      $data['payment_pagseguro_campo_numero'] = $this->request->post['payment_pagseguro_campo_numero'];
    } else {
      $data['payment_pagseguro_campo_numero'] = $this->config->get('payment_pagseguro_campo_numero');
    }

    if (isset($this->request->post['payment_pagseguro_campo_complemento'])) {
      $data['payment_pagseguro_campo_complemento'] = $this->request->post['payment_pagseguro_campo_complemento'];
    } else {
      $data['payment_pagseguro_campo_complemento'] = $this->config->get('payment_pagseguro_campo_complemento');
    }

    if (isset($this->request->post['payment_pagseguro_campo_ddd'])) {
      $data['payment_pagseguro_campo_ddd'] = $this->request->post['payment_pagseguro_campo_ddd'];
    } else {
      $data['payment_pagseguro_campo_ddd'] = $this->config->get('payment_pagseguro_campo_ddd');
    }

    if (isset($this->request->post['payment_pagseguro_campo_celular'])) {
      $data['payment_pagseguro_campo_celular'] = $this->request->post['payment_pagseguro_campo_celular'];
    } else {
      $data['payment_pagseguro_campo_celular'] = $this->config->get('payment_pagseguro_campo_celular');
    }

    if (isset($this->request->post['payment_pagseguro_campo_cpf'])) {
      $data['payment_pagseguro_campo_cpf'] = $this->request->post['payment_pagseguro_campo_cpf'];
    } else {
      $data['payment_pagseguro_campo_cpf'] = $this->config->get('payment_pagseguro_campo_cpf');
    }

    if (isset($this->request->post['payment_pagseguro_campo_cnpj'])) {
      $data['payment_pagseguro_campo_cnpj'] = $this->request->post['payment_pagseguro_campo_cnpj'];
    } else {
      $data['payment_pagseguro_campo_cnpj'] = $this->config->get('payment_pagseguro_campo_cnpj');
    }

    if (isset($this->request->post['payment_pagseguro_name'])) {
      $data['payment_pagseguro_name'] = $this->request->post['payment_pagseguro_name'];
    } else if ($this->config->get('payment_pagseguro_name')) {
      $data['payment_pagseguro_name'] = $this->config->get('payment_pagseguro_name');
    } else {
      $data['payment_pagseguro_name'] = $this->language->get('text_default_name');
    }

/*     if (isset($this->request->post['payment_pagseguro_chave'])) {
      $data['payment_pagseguro_chave'] = $this->request->post['payment_pagseguro_chave'];
    } else {
      $data['payment_pagseguro_chave'] = $this->config->get('payment_pagseguro_chave') || "";
    }
 */
    // configurações individuais

    if (isset($this->request->post['payment_pagseguro_boleto_status'])) {
      $data['payment_pagseguro_boleto_status'] = $this->request->post['payment_pagseguro_boleto_status'];
    } else {
      $data['payment_pagseguro_boleto_status'] = $this->config->get('payment_pagseguro_boleto_status');
    }

    if (isset($this->request->post['payment_pagseguro_boleto_desconto'])) {
      $data['payment_pagseguro_boleto_desconto'] = $this->request->post['payment_pagseguro_boleto_desconto'];
    } else {
      $data['payment_pagseguro_boleto_desconto'] = $this->config->get('payment_pagseguro_boleto_desconto');
    }

    if (isset($this->request->post['payment_pagseguro_cartao_credito_status'])) {
      $data['payment_pagseguro_cartao_credito_status'] = $this->request->post['payment_pagseguro_cartao_credito_status'];
    } else {
      $data['payment_pagseguro_cartao_credito_status'] = $this->config->get('payment_pagseguro_cartao_credito_status');
    }

    if (isset($this->request->post['payment_pagseguro_cartao_credito_desconto'])) {
      $data['payment_pagseguro_cartao_credito_desconto'] = $this->request->post['payment_pagseguro_cartao_credito_desconto'];
    } else {
      $data['payment_pagseguro_cartao_credito_desconto'] = $this->config->get('payment_pagseguro_cartao_credito_desconto');
    }

    if (isset($this->request->post['payment_pagseguro_cartao_credito_qtde_max_parcelas'])) {
      $data['payment_pagseguro_cartao_credito_qtde_max_parcelas'] = $this->request->post['payment_pagseguro_cartao_credito_qtde_max_parcelas'];
    } else {
      $data['payment_pagseguro_cartao_credito_qtde_max_parcelas'] = $this->config->get('payment_pagseguro_cartao_credito_qtde_max_parcelas');
    }

    if (isset($this->request->post['payment_pagseguro_cartao_credito_qtde_parcelas_sem_juros'])) {
      $data['payment_pagseguro_cartao_credito_qtde_parcelas_sem_juros'] = $this->request->post['payment_pagseguro_cartao_credito_qtde_parcelas_sem_juros'];
    } else {
      $data['payment_pagseguro_cartao_credito_qtde_parcelas_sem_juros'] = $this->config->get('payment_pagseguro_cartao_credito_qtde_parcelas_sem_juros');
    }

    if (isset($this->request->post['payment_pagseguro_deposito_status'])) {
      $data['payment_pagseguro_deposito_status'] = $this->request->post['payment_pagseguro_deposito_status'];
    } else {
      $data['payment_pagseguro_deposito_status'] = $this->config->get('payment_pagseguro_deposito_status');
    }

    if (isset($this->request->post['payment_pagseguro_deposito_desconto'])) {
      $data['payment_pagseguro_deposito_desconto'] = $this->request->post['payment_pagseguro_deposito_desconto'];
    } else {
      $data['payment_pagseguro_deposito_desconto'] = $this->config->get('payment_pagseguro_deposito_desconto');
    }

    if (isset($this->request->post['payment_pagseguro_debito_online_status'])) {
      $data['payment_pagseguro_debito_online_status'] = $this->request->post['payment_pagseguro_debito_online_status'];
    } else {
      $data['payment_pagseguro_debito_online_status'] = $this->config->get('payment_pagseguro_debito_online_status');
    }

    if (isset($this->request->post['payment_pagseguro_debito_online_desconto'])) {
      $data['payment_pagseguro_debito_online_desconto'] = $this->request->post['payment_pagseguro_debito_online_desconto'];
    } else {
      $data['payment_pagseguro_debito_online_desconto'] = $this->config->get('payment_pagseguro_debito_online_desconto');
    }

    if (isset($this->request->post['payment_pagseguro_saldo_pagseguro_status'])) {
      $data['payment_pagseguro_saldo_pagseguro_status'] = $this->request->post['payment_pagseguro_saldo_pagseguro_status'];
    } else {
      $data['payment_pagseguro_saldo_pagseguro_status'] = $this->config->get('payment_pagseguro_saldo_pagseguro_status');
    }

    if (isset($this->request->post['payment_pagseguro_saldo_pagseguro_desconto'])) {
      $data['payment_pagseguro_saldo_pagseguro_desconto'] = $this->request->post['payment_pagseguro_saldo_pagseguro_desconto'];
    } else {
      $data['payment_pagseguro_saldo_pagseguro_desconto'] = $this->config->get('payment_pagseguro_saldo_pagseguro_desconto');
    }

    if (isset($this->request->post['payment_pagseguro_envia_endereco_entrega'])) {
      $data['payment_pagseguro_envia_endereco_entrega'] = $this->request->post['payment_pagseguro_envia_endereco_entrega'];
    } else {
      $data['payment_pagseguro_envia_endereco_entrega'] = $this->config->get('payment_pagseguro_envia_endereco_entrega');
    }

    $this->load->model('customer/custom_field');
    $data['custom_fields'] = $this->model_customer_custom_field->getCustomFields();

    $data['help_campos_personalizados'] = sprintf($this->language->get('help_campos_personalizados'), $this->session->data['user_token']);

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');
    $this->response->setOutput($this->load->view('extension/payment/pagseguro', $data));
  }

  protected function validate()
  {
    if (!$this->user->hasPermission('modify', 'extension/payment/pagseguro')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if (!$this->request->post['payment_pagseguro_token']) {
      $this->error['token'] = $this->language->get('error_token');
    }

    if (!$this->request->post['payment_pagseguro_email']) {
      $this->error['email'] = $this->language->get('error_email');
    }

    /*        if (!isset($this->request->post['payment_pagseguro_chave']) || (utf8_strlen(trim($this->request->post['payment_pagseguro_chave'])) != 24)) {
           $this->error['chave'] = $this->language->get('error_chave');
       } */

    $result = $this->validar($this->request->post['payment_pagseguro_chave'] || "");

    if ($result['error']) {
      $this->error['result'] = $result['error'];
    }

    return !$this->error;
  }

  public function install()
  {
    $this->uninstall();

    $this->load->model('setting/setting');

    $data = array();

    $data['payment_pagseguro_boleto_status'] = '1';
    $data['payment_pagseguro_cartao_credito_status'] = '1';
    $data['payment_pagseguro_deposito_status'] = '1';
    $data['payment_pagseguro_debito_online_status'] = '1';
    $data['payment_pagseguro_saldo_pagseguro_status'] = '1';

    $this->model_setting_setting->editSetting('payment_pagseguro', $data);

    $this->hideDeprecatedErrors();
  }

  private function hideDeprecatedErrors()
  {
    $arquivo = DIR_SYSTEM . 'framework.php';

    if ((version_compare(phpversion(), '5.4', '>=') == true) && file_exists($arquivo) && is_writeable($arquivo)) {
      $data = file_get_contents($arquivo);

      if ($data) {
        $find[] = 'if ($config->get(\'error_display\')) {';
        $replace[] = 'if ($config->get(\'error_display\') && $error != \'Unknown\') {';

        $find[] = 'if ($config->get(\'error_log\')) {';
        $replace[] = 'if ($config->get(\'error_log\') && $error != \'Unknown\') {';

        $data = str_replace($find, $replace, $data, $count);

        if ($count > 0) {
          file_put_contents($arquivo, $data);
        }
      }
    }
  }

  public function uninstall()
  {
    $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = 'payment_pagseguro'");
  }

  protected function validar($license_key = "")
  {
    return array(
      'error'        => false,
      'success'      => true
    );
  }
}
